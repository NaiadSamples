<?php
/*
 * Copyright (c) 2015      Marcin Cieślak (saper.info) 
 * 
 * Naiad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Naiad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Naiad.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * Naiad skin extension
 *
 * @file
 * @ingroup Extensions
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License 3.0 or later
*/


class NaiadSamples {

	public static function NavTree( &$value ) {
		wfDebugLog( 'naiad', __METHOD__ . ": received [$value] ");
		if ( $value === '' ) {
			$value = '{{#tree:id=main-index|root=MediaWiki index|
* [[Main_Page|Main Page]]
* [[Special:RecentChanges|Recent Changes]]
* [[Special:Categories|Categories]]
}}';
			return true;
		}
	}

	public static function NavTreeTopPages( &$value ) {
		wfDebugLog( 'naiad', __METHOD__ . ": received [$value] ");
		if ( $value === '' ) {
			$value = "<div id=\"navtree_selector\">\n<div class=\"dd_button\"><p>{{#switch:{{NAMESPACE}}\n|Special|File={{NAMESPACE}} Page\n|{{FULLPAGENAME}}}}</p></div>\n<div class=\"dd_content\">\n* [[Main Page|Main Page]]\n* [[Special:RecentChanges|Recent Changes]]\n*<span>Page lists</span>\n* [[Special:AllPages|All pages]]\n* [[Special:Categories|All categories]]\n</div>\n</div>";
			return true;
		}
	}

	public static function Languages( &$value ) {
		wfDebugLog( 'naiad', __METHOD__ . ": received [$value] ");
		if ( $value === '' ) {
			$value = '<div id="sel_lan" class="dd_item">
<div class="button grey">
English
</div>
<div class="dd_menu languages">
* [[:{{FULLPAGENAME}}|English]]
* [[:{{NAMESPACE}}:DE/{{PAGENAME}}|Deutsch]]
</div>';
			return true;
		}
	}

	public static function Series( &$value ) {
		wfDebugLog( 'naiad', __METHOD__ . ": received [$value] ");
		if ( $value === '' ) {
			$value = '<div id="sel_ser" class="dd_item">
<div class="button grey">
Version 2.0
</div> 
<div class="dd_menu series">   
* Version 1.0
* Version 2.0
</div>';
			return true;
		}
	}

	public static function BuildSidebar ( $skin, &$bar ) {
		wfDebugLog( 'naiad', 'Sidebar!' . var_export($bar, true));
		if ( !isset( $bar[ 'maintenance' ] ) ) {
			$bar = $skin->addToSidebarPlain( $bar, "
* maintenance
**http://projects.blender.org/tracker/?atid=510&group_id=163&func=browse | Report a wiki bug
**Meta:Guides/Writer_Guide|Wiki Guidelines
*monitoring
**Special:Categories|Categories
**Special:Popularpages|Popular pages
**Special:Newimages|New files
**Special:Newpages|New pages
**recentchanges-url|recentchanges

*navigation
**http://www.blender.org | blender.org | Don't go to blender.org website
**http://code.blender.org | code.blender.org | Don't go to blender development blog");
			return true;
		}
	}
}
